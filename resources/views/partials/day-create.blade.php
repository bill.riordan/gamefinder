<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="{{ asset('js/multidate/jquery-ui.multidatespicker.js') }}"></script>
    
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/overcast/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <link rel="stylesheet" href="{{ asset('js/multidate/jquery-ui.multidatespicker.css') }}">

<style>
.ui-state-highlight .ui-widget-content .ui-state-highlight {
	border-color: #222 !important;
}
</style>

<div class="modal fade" id="AddDaysModal" tabindex="-1" role="dialog" aria-labelledby="AddDaysModal" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content text-center">
			<div class="modal-header">
			 	<h5 class="modal-title" id="exampleModalLabel">Add Days</h5>
			 		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			 			<span aria-hidden="true">&times;</span>
			 		</button>
			</div>
			<div class="modal-body">
        		{{ Form::open(array('url' => 'day/store')) }}
					<input id="date" type="text" name="dates" class="readonly" required>
					<button type="submit" class="btn btn-primary">
                        {{ __('Accept') }}
                    </button>
				{{ Form::close() }}
			</div>
		</div>
	</div>
</div>



<div class='col-md-4'></div>

<script>
    $(".readonly").on('keydown paste', function(e){
        e.preventDefault();
    });
	$('#date').multiDatesPicker({dateFormat: "yy-mm-dd", minDate: 0});
</script>